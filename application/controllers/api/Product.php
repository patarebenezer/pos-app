<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use chriskacerguis\RestServer\RestController;

class Product extends RestController
{
    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        parent::__construct();
    }
    
    public function index_get($id = 0)
    {
        $check = $this->db->get_where('product', ['id' => $id])->row_array();
        if ($id) {
            if ($check) {
                $this->response($check, 200);
            } else {
                $this->response([
                    'status' => false,
                    'message' => 'Data not found'
                ], 404);
            }
        } else {
            $data = $this->db->get('product')->result();
            $this->response($data, 200);
        }
    }

    public function index_post()
    {
        $data = [
            'product_name' => $this->post('product_name'),
            'product_desc' => $this->post('product_desc'),
            'product_price' => $this->post('product_price'),
            'product_kategori' => $this->post('product_kategori'),
            'product_img' => $this->post('product_img')
        ];
        $insert_data = $this->db->insert('product', $data);
        if ($insert_data) {
            $this->response($data, 200);
        } else {
            $this->response([
                'status' => 'failed',
                502
            ]);
        }
    }

    public function index_put()
    {
        $id = $this->put('id');
        $data = [
            'product_name' => $this->put('product_name'),
            'product_desc' => $this->put('product_desc'),
            'product_price' => $this->put('product_price'),
            'product_kategori' => $this->put('product_kategori'),
            'product_img' => $this->put('product_img')
        ];
        $this->db->where('id', $id);
        $update_data = $this->db->update('product', $data);
        if ($update_data) {
            $this->response($data, 200);
        } else {
            $this->response([
                'status' => 'failed',
                502
            ]);
        }
    }

    public function index_delete()
    {
        $id = $this->delete('id');
        $check = $this->db->get_where('product', ['id' => $id])->row_array();
        if ($check) {
            $this->db->where('id', $id);
            $this->db->delete('product');
            $this->response(['status' => 'Data deleted successfully'], 200);
        } else {
            $this->response(['status' => 'Data not found'], 404);
        }
    }
}
