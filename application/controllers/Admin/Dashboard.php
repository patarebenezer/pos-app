<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
session_start();
class Dashboard extends CI_Controller
{

    var $userdata;
    function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('userdata')) {
            redirect('auth');
        }
        $this->userdata = $this->session->userdata('userdata');
    }

    public function index()
    {
        $data['username'] = $this->userdata->username;
        $this->load->view('admin/vw_header', $data);
        $this->load->view('admin/vw_dashboard');
        $this->load->view('admin/vw_footer');
    }

    public function list()
    {
        $data['username'] = $this->userdata->username;
        $this->load->view('admin/vw_header', $data);
        $this->load->view('admin/vw_list');
        $this->load->view('admin/vw_footer');
    }

    public function add()
    {
        $data['username'] = $this->userdata->username;
        $data['category'] = $this->db->query("SELECT * FROM category")->result();
        $this->load->view('admin/vw_add', $data);
    }

    public function save()
    {
        $data['category'] = $this->db->query("SELECT * FROM category")->result();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('product_name', 'Name', 'required|min_length[5]');
        $this->form_validation->set_rules('product_price', 'Price', 'required|numeric|max_length[15]');
        $this->form_validation->set_rules('product_kategori', 'Category', 'required');
        $this->form_validation->set_rules('product_img', 'Image', 'required');
        $this->form_validation->set_rules('product_desc', 'Description', 'required|min_length[5]');
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

        if ($this->form_validation->run() == false) {
            $this->load->view('admin/vw_add', $data);
        } else {
            $data = [
                'product_name' => $this->input->post('product_name'),
                'product_kategori' => $this->input->post('product_kategori'),
                'product_desc' => $this->input->post('product_desc'),
                'product_price' => $this->input->post('product_price'),
                'product_img' => $this->input->post('product_img')
            ];
            $this->db->insert('product', $data);
            redirect('admin/dashboard');
        }
    }

    public function edit()
    {
        $id = $this->uri->segment(4);
        $data['data'] = $this->db->query("SELECT * FROM product WHERE id='$id'")->row();
        $data['category'] = $this->db->query("SELECT * FROM category")->result();
        $this->load->view('admin/vw_edit', $data);
    }

    public function update()
    {
        $id = $this->input->post('id');
        $data = [
            'product_name' => $this->input->post('product_name'),
            'product_kategori' => $this->input->post('product_kategori'),
            'product_desc' => $this->input->post('product_desc'),
            'product_price' => $this->input->post('product_price'),
            'product_img' => $this->input->post('product_img')
        ];
        // print_r($data);
        // print_r($id);
        // exit;
        $this->db->where('id', $id);
        $this->db->update('product', $data);
        // return true;
        redirect('admin/dashboard');
    }
}
