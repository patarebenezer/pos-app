<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
session_start();

class Dashboard extends CI_Controller
{

    var $userdata;
    function __construct()
    {
        parent::__construct();
        if (!$this->session->has_userdata('userdata')) {
            redirect('auth');
        }
        $this->userdata = $this->session->userdata('userdata');
    }

    public function index()
    {
        $data['username'] = $this->userdata->username;
        $this->load->view('vw_dashboard', $data);
    }
    
}
