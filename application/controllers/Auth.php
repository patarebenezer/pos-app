<?php
defined('BASEPATH') or exit('No direct script access allowed');
session_start();
class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("M_Login");
    }

    public function index()
    {
        $cek_session = $this->session->userdata("userdata");
        if ($cek_session->level == "client") {
            redirect('dashboard');
        } elseif($cek_session->level == "admin") {
            redirect('admin/dashboard');
        }else{
            $this->load->view('vw_auth');
        }
    }

    public function cek_login()
    {
        $data = array(
            'username' => $this->input->post('username', TRUE),
            'password' => md5($this->input->post('password', TRUE))
        );

        $hasil = $this->M_Login->login($data);
        if ($hasil->num_rows() == 1) {
            $userdata = $hasil->row();
            $this->session->set_userdata('userdata', $userdata);
            
            if ($userdata->level == 'admin') {
                redirect('admin/dashboard');
            } elseif ($userdata->level == 'client') {
                redirect('dashboard');
            } else {
                var_dump($userdata);
            }
        } else {
            echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
        }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('auth');
    }
}
