<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Client</title>
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body class="relative mb-[200px]">
    <header class="flex justify-between py-4 px-8 w-full bg-gray-700">
        <p class="text-white text-lg font-bold">Majoo Teknologi Indonesia</p>
        <p class="text-white capitalize">Welcome, <a href="<?=site_url()?>/auth/logout" class="hover:underline hover:cursor-pointer"><?= $username;?></a></p>
    </header>
    <section>
        <h1 class="px-8 text-4xl py-3 font-bold">Product</h1>
        <div class="grid md:grid-cols-2 lg:grid-cols-4 px-8 gap-5" id="product">
        </div>
    </section>
    <footer class="py-4 mt-14 bg-gray-700 text-white bottom-0 w-full text-center font-light fixed bottom-0">2022 - PT Majoo Teknologi Indonesia</footer>
</body>

<!-- Script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type='text/javascript'>
    $(document).ready(function() {
        view_product();

        function view_product() {
            $.ajax({
                type: 'GET',
                url: 'http://127.0.0.1:8080/api/product/',
                async: true,
                dataType: 'json',
                success: function(data) {
                    var html = '';
                    var i;
                    var base = "<?= base_url() ?>/assets/";
                    var urls = 'http://127.0.0.1:8080/api/product/'
                    for (i = 0; i < data.length; i++) {
                        html += '<div class="border rounded-xl hover:bg-gray-50 px-4 py-6 leading-[25px]">' +
                            '<img src="' + base + data[i].product_img + '" alt="" class="w-full">' +
                            '<p class="font-bold text-xl text-center mb-2">' + data[i].product_name + '</p>' +
                            '<p class="font-bold text-lg text-center mb-4">' + new Intl.NumberFormat('IN', {
                                maximumSignificantDigits: 3
                            }).format(data[i].product_price) + '</p>' +
                            '<p class="text-center font-light text-gray-500">' + data[i].product_desc + '</p>' +
                            '<div class="flex justify-center">' +
                            '<a href="' + urls + data[i].id + '" class="rounded-md w-1/2 text-center mt-4 bg-green-500 text-white hover:bg-green-600 py-1.5 font-bold text-lg">' + 'Buy' + '</a>' +
                            '</div>' +
                            '</div>';
                    }
                    $('#product').html(html);
                }

            });
        }
    });
</script>

</html>