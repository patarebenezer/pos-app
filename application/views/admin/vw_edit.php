<?php $this->load->view('admin/vw_header') ?>
<section class="mx-auto my-6 rounded-md px-4 py-2 w-10/12">
    <div class="flex gap-4">
        <div class="w-[350px] border">
            <h1 class="text-center pt-4 text-2xl font-bold text-gray-700">POS Mini <br> System</h1>
            <nav class="px-5 py-7 grid">
                <a href="<?= site_url() ?>/admin/dashboard/add" class="bg-gray-400 text-white rounded-xl px-4 py-2 text-lg font-semibold shadow mb-5 hover:shadow-md hover:bg-green-500">
                    <div class="flex justify-between items-center">
                        <p>Edit Product</p>
                        <i class="fa fa-pencil-square-o text-xl"></i>
                    </div>
                </a>
                <a href="<?= site_url() ?>/admin/dashboard/list" class="bg-gray-400 text-white rounded-xl px-4 py-2 text-lg font-semibold mb-5 shadow hover:shadow-md hover:bg-green-500">
                    <div class="flex justify-between items-center">
                        <p>List Product</p>
                        <i class="fa fa-tasks text-xl"></i>
                    </div>
                </a>
            </nav>
        </div>
        <div class="w-full border text-xl font-bold text-gray-700">
            <h1 class="text-right bg-green-700 text-white py-2 pr-4">Edit Product <i class="fa fa-plus-square"></i></h1>
            <?php echo form_open('admin/dashboard/update/', array("id" => "form-user")); ?>
            <div class="grid px-8 py-6">
                <div id="the-message"></div>
                <input type="text" value="<?=$data->id?>" class="hidden" name="id"/>
                <div class="flex gap-5 w-full px-4 py-3 justify-between">
                    <div class="mb-3 xl:w-96">
                        <label for="product-name" class="text-[16px] font-light form-label inline-block text-gray-400">Product Name</label>
                        <input type="text" value="<?=$data->product_name?>" id="product_name" class="form-control block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-green-600 focus:outline-none" placeholder="Product Name" name="product_name" />
                        <?php echo form_error('product_name', '<div class="font-light text-sm text-red-500">', '</div>'); ?>
                    </div>
                    <div class="mb-3 xl:w-96">
                        <label for="product-price" class="text-[16px] font-light form-label inline-block text-gray-400">Price</label>
                        <input type="number"  value="<?=$data->product_price?>" id="product_price" class="form-control block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-green-600 focus:outline-none" placeholder="Price" name="product_price" />
                        <?php echo form_error('product_price', '<div class="font-light text-sm text-red-500">', '</div>'); ?>
                    </div>
                </div>

                <div class="gap-5 w-full px-4">
                    <div class="mb-1 xl:w-full">
                        <label for="exampleFormControlTextarea1" class="text-[16px] font-light form-label inline-block text-gray-400">Description</label>
                        <textarea id="product_desc" class=" form-control block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-green-600 focus:outline-none" id="exampleFormControlTextarea1" rows="3" placeholder="Description Product" name="product_desc">
                            <?= $data->product_desc?>
                        </textarea>
                        <?php echo form_error('product_desc', '<div class="font-light text-sm text-red-500">', '</div>'); ?>
                    </div>
                </div>
            </div>


            <div class="mb-8 grid grid-cols-2 px-8 gap-5 w-full">
                <div class="w-full">
                    <label for="product_kategori" class="block mb-2 text-[16px] px-4 font-light text-gray-400">Select Category</label>
                    <select id="product_kategori" value="<?=$data->product_kategori?>"  class="bg-gray-50 w-11/12 font-normal border mx-4 border-gray-300 text-gray-600 text-[15px] rounded-lg focus:ring-green-500 capitalize focus:border-green-500 block w-full p-2.5" name="product_kategori">
                        <option class="font-normal">Category Product</option>
                        <?php foreach ($category as $value) : ?>
                            <option class="" <?php echo $data->product_kategori === $value->id ? 'selected' : ''?> value="<?= $value->id ?>"><?= $value->category_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?php echo form_error('product_kategori', '<div class="font-light text-sm text-red-500">', '</div>'); ?>
                </div>

                <div class="w-full px-4">
                    <label class="block mb-2 text-[16px] font-light text-gray-400" for="file_input">Upload file</label>
                    <input id="product_img"  value="<?=$data->product_img?>" class="block w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 cursor-pointer dark:text-gray-400 focus:outline-none px-3 py-2" aria-describedby="file_input_help" id="file_input" type="file" name="product_img">
                    <?php echo form_error('product_img', '<div class="font-light text-sm text-red-500">', '</div>'); ?>
                </div>

            </div>
            <div class="px-12 pt-3 pb-10 w-full flex justify-center">
                <button type="submit" id="btn_simpan" class="w-full rounded-lg hover:bg-green-600 shadow-lg font-semibold text-[16px] bg-green-500 text-white px-3 py-2">
                    <div class="flex items-center justify-center gap-2">
                        <p>Update</p>
                        <i class="fa fa-rocket"></i>
                    </div>
                </button>
            </div>
            <?php echo form_close(); ?>

        </div>
    </div>
</section>
<?php $this->load->view('admin/vw_footer'); ?>