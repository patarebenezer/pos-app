<?php echo form_open('admin/dashboard/save/', array("id" => "form-user")); ?>
<div class="grid px-8 py-6">
    <div id="the-message"></div>
    <div class="flex gap-5 w-full px-4 py-3 justify-between">
        <div class="mb-3 xl:w-96">
            <label for="product-name" class="text-[16px] font-light form-label inline-block text-gray-400">Product Name</label>
            <input type="text" id="product_name" class="form-control block w-full px-3 py-1.5 text-base font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-green-600 focus:outline-none" placeholder="Product Name" name="product_name" />
            <?php echo form_error('product_name', '<div class="font-light text-sm text-red-500">', '</div>'); ?>
        </div>
        <div class="mb-3 xl:w-96">
            <label for="product-price" class="text-[16px] font-light form-label inline-block text-gray-400">Price</label>
            <input type="number" id="product_price" class="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-green-600 focus:outline-none
      " placeholder="Price" name="product_price" />
            <?php echo form_error('product_price', '<div class="font-light text-sm text-red-500">', '</div>'); ?>
        </div>
    </div>

    <div class="gap-5 w-full px-4">
        <div class="mb-1 xl:w-full">
            <label for="exampleFormControlTextarea1" class="text-[16px] font-light form-label inline-block text-gray-400">Description</label>
            <textarea id="product_desc" class="
        form-control
        block
        w-full
        px-3
        py-1.5
        text-base
        font-normal
        text-gray-700
        bg-white bg-clip-padding
        border border-solid border-gray-300
        rounded
        transition
        ease-in-out
        m-0
        focus:text-gray-700 focus:bg-white focus:border-green-600 focus:outline-none
      " id="exampleFormControlTextarea1" rows="3" placeholder="Description Product" name="product_desc"></textarea>
            <?php echo form_error('product_desc', '<div class="font-light text-sm text-red-500">', '</div>'); ?>
        </div>
    </div>
</div>


<div class="mb-8 grid grid-cols-2 px-8 gap-5 w-full">
    <div class="w-full">
        <label for="product_kategori" class="block mb-2 text-[16px] px-4 font-light text-gray-400">Select Category</label>
        <select id="product_kategori" class="bg-gray-50 w-11/12 font-normal border mx-4 border-gray-300 text-gray-600 text-[15px] rounded-lg focus:ring-green-500 capitalize focus:border-green-500 block w-full p-2.5" name="product_kategori">
            <option selected="" class="font-normal">Category Product</option>
            <?php foreach ($category as $value) : ?>
                <option class="" value="<?= $value->id ?>"><?= $value->category_name; ?></option>
                <?php endforeach; ?>
            </select>
            <?php echo form_error('product_kategori', '<div class="font-light text-sm text-red-500">', '</div>'); ?>
    </div>

    <div class="w-full px-4">
        <label class="block mb-2 text-[16px] font-light text-gray-400" for="file_input">Upload file</label>
        <input id="product_img" class="block w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 cursor-pointer dark:text-gray-400 focus:outline-none px-3 py-2" aria-describedby="file_input_help" id="file_input" type="file" name="product_img">
        <?php echo form_error('product_img', '<div class="font-light text-sm text-red-500">', '</div>'); ?>
    </div>

</div>
<div class="px-12 pt-3 pb-10 w-full flex justify-center">
    <button type="submit" id="btn_simpan" class="w-full rounded-lg hover:bg-green-600 shadow-lg font-semibold text-[16px] bg-green-500 text-white px-3 py-2">
        <div class="flex items-center justify-center gap-2">
            <p>Add</p>
            <i class="fa fa-rocket"></i>
        </div>
    </button>
</div>
<?php echo form_close(); ?>

<script>
    $('#form-user').submit(function(e) {
        e.preventDefault();
        var me = $(this);
        $.ajax({
            url: me.attr('action'),
            type: 'post',
            data: me.serialize(),
            dataType: 'json',
            success: function(response) {
                if (response.success == true) {
                    alert('success');
                } else {
                    $.each(response.messages, function(key, value) {
                        var element = $('#' + key);
                        element.after(value)
                    })
                }
            }
        })
    })

    function view_product() {
        $.ajax({
            type: 'GET',
            url: 'http://127.0.0.1:8080/api/product/',
            async: true,
            dataType: 'json',
            success: function(data) {
                var html = '';
                var i;
                var base = "<?= base_url() ?>/assets/";
                var urls = 'http://127.0.0.1:8080/api/product/'
                for (i = 0; i < data.length; i++) {
                    html += '<tr class="hover:bg-gray-100">' +
                        '<td class="!border font-semibold">' + data[i].product_name + '</td>' +
                        '<td class="!border">' + data[i].product_price + '</td>' +
                        '<td style="text-align:center;" class="!border">' +
                        '<a href="#" class="text-blue-500 text-center" data="' + data[i].id + '">Edit</a>' + ' ' +
                        '</td>' +
                        '</tr>';
                }
                $('#product').html(html);
            }

        });
    }

    // Save data
    $('#btn_simpan').on('click', function() {
        var product_name = $('#product_name').val();
        var product_desc = $('#product_desc').val();
        var product_img = $('#product_img').val();
        var product_kategori = $('#product_kategori').val();
        var product_price = $('#product_price').val();
        $.ajax({
            type: "POST",
            url: "http://127.0.0.1:8080/api/product/",
            dataType: "JSON",
            data: {
                product_name: product_name,
                product_desc: product_desc,
                product_img: product_img,
                product_kategori: product_kategori,
                product_price: product_price,
            },
            success: function(data) {
                $('[name="product_name"]').val("");
                $('[name="product_desc"]').val("");
                $('[name="product_img"]').val("");
                $('[name="product_kategori"]').val("");
                $('[name="product_price"]').val("");
                view_product();
            }
        });
        return false;
    });
</script>