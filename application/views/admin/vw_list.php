</head>
<section class="mx-auto my-6 rounded-md px-4 py-2 w-10/12">
    <div class="flex gap-4">
        <div class="w-[350px] border">
            <h1 class="text-center pt-4 text-2xl font-bold text-gray-700">POS Mini <br> System</h1>
            <nav class="px-5 py-7 grid">
                <a href="<?= site_url() ?>/admin/dashboard/add" class="bg-gray-400 text-white rounded-xl px-4 py-2 text-lg font-semibold shadow mb-5 hover:shadow-md hover:bg-green-500">
                    <div class="flex justify-between items-center">
                        <p>Add Product</p>
                        <i class="fa fa-pencil-square-o text-xl"></i>
                    </div>
                </a>
                <a href="<?= site_url() ?>/admin/dashboard/list" class="bg-gray-400 text-white rounded-xl px-4 py-2 text-lg font-semibold mb-5 shadow hover:shadow-md hover:bg-green-500">
                    <div class="flex justify-between items-center">
                        <p>List Product</p>
                        <i class="fa fa-tasks text-xl"></i>
                    </div>
                </a>
            </nav>
        </div>
        <div class="w-full border text-xl font-bold text-sm text-gray-700">
            <h1 class="text-right bg-green-700 text-white !text-[18px] py-3 pr-4 mb-[30px]">List Product</h1>
            <div class="px-6 py-4">
                <table class="table-auto " id="tableproduct">
                    <thead>
                        <tr class="text-sm">
                            <th>Product Name</th>
                            <th>Price</th>
                            <th style="text-align: right;">Aksi</th>
                        </tr>
                    </thead>
                    <tbody id="product" class="font-light text-[16px]">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function () {
    $('#tableproduct').DataTable({
        serverSide: true,
        processing: true,
        serverSide: true,
        ajax: 'http://127.0.0.1:8080/api/product/',
        
    });
});
</script>
<script type='text/javascript'>
    $(document).ready(function() {
        view_product();

        function view_product() {
            $.ajax({
                type: 'GET',
                url: 'http://127.0.0.1:8080/api/product/',
                async: true,
                dataType: 'json',
                success: function(data) {
                    var html = '';
                    var i;
                    var base = "<?= base_url() ?>admin/dashboard/edit/";
                    var urls = 'http://127.0.0.1:8080/api/product/'
                    for (i = 0; i < data.length; i++) {
                        html += '<tr class="hover:bg-gray-100">' +
                            '<td class="!border font-semibold">' + data[i].product_name + '</td>' +
                            '<td class="!border">' + data[i].product_price + '</td>' +
                            '<td style="text-align:center;" class="!border">' +
                            '<a href="'+base+data[i].id+'" class="text-blue-500 text-center" data="' + data[i].id + '">Edit</a>' + ' ' +
                            '</td>' +
                            '</tr>';
                    }
                    $('#product').html(html);
                }

            });
        }
    });
</script>