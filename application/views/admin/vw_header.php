<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
    <script src="https://cdn.tailwindcss.com"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" />

    <style>
        .underlines {
            position: absolute;
            bottom: -3px;
            width: 25%;
            background-color: #E94560;
            height: 5px;
        }
    </style>
</head>

<body class="relative mb-[200px]">
    <header class="flex justify-between py-4 px-8 w-full bg-green-400 shadow-lg">
        <div class="relative flex">
            <a href="<?= site_url() ?>/admin/dashboard" class="text-white text-xl font-bold uppercase">Majoo Teknologi Indonesia</a>
            <span class="underlines rounded-xl"></span>
        </div>
        <p class="text-white capitalize">Welcome, <a href="<?= site_url() ?>/auth/logout" class="hover:underline hover:cursor-pointer"><?= $username; ?></a></p>
    </header>