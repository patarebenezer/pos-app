<?php $this->load->view('admin/vw_header')?>
<section class="mx-auto my-6 rounded-md px-4 py-2 w-10/12">
    <div class="flex gap-4">
        <div class="w-[350px] border">
            <h1 class="text-center pt-4 text-2xl font-bold text-gray-700">POS Mini <br> System</h1>
            <nav class="px-5 py-7 grid">
                <a href="<?= site_url() ?>/admin/dashboard/add" class="bg-gray-400 text-white rounded-xl px-4 py-2 text-lg font-semibold shadow mb-5 hover:shadow-md hover:bg-green-500">
                    <div class="flex justify-between items-center">
                        <p>Add Product</p>
                        <i class="fa fa-pencil-square-o text-xl"></i>
                    </div>
                </a>
                <a href="<?= site_url() ?>/admin/dashboard/list" class="bg-gray-400 text-white rounded-xl px-4 py-2 text-lg font-semibold mb-5 shadow hover:shadow-md hover:bg-green-500">
                    <div class="flex justify-between items-center">
                        <p>List Product</p>
                        <i class="fa fa-tasks text-xl"></i>
                    </div>
                </a>
            </nav>
        </div>
        <div class="w-full border text-xl font-bold text-gray-700">
            <h1 class="text-right bg-green-700 text-white py-2 pr-4">Add Product <i class="fa fa-plus-square"></i></h1>
            <?php $this->load->view('admin/vw_form');?>
        </div>
    </div>
</section>
<?php $this->load->view('admin/vw_footer');?>