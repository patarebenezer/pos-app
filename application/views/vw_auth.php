<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Auth</title>
	<script src="https://cdn.tailwindcss.com"></script>
</head>

<body>
	<div class="lg:flex">
		<div class="lg:w-1/2 xl:max-w-screen-sm">
			<div class="py-12 bg-cyan-100 lg:bg-white flex justify-center lg:justify-start lg:px-12">
				<div class="cursor-pointer flex items-center">

					<div class="text-2xl text-cyan-800 tracking-wide ml-2 font-semibold">
						<a href="/">Home</a>
					</div>
				</div>
			</div>
			<div class="mt-10 px-12 sm:px-24 md:px-48 lg:px-12 lg:mt-16 xl:px-24 xl:max-w-2xl">
				<h2 class="text-center text-4xl text-cyan-900 font-display font-semibold lg:text-left xl:text-5xl
              xl:text-bold">
					Log in
				</h2>
				<div class="mt-12">
					<?php echo form_open('auth/cek_login/'); ?>
					<div>
						<div class="text-sm font-bold text-gray-700 tracking-wide">
							Username
						</div>
						<input class="w-full px-3 rounded text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-cyan-500" type="input" placeholder="admin" name="username" required />

					</div>
					<div class="mt-8">
						<div class="flex justify-between items-center">
							<div class="text-sm font-bold text-gray-700 tracking-wide">
								Password
							</div>
							<div>
								<a class="text-xs font-display font-semibold text-cyan-600 hover:text-cyan-800
                                  cursor-pointer">
									Forgot Password?
								</a>
							</div>
						</div>
						<input class="w-full px-3 rounded text-lg py-2 border-b border-gray-300 focus:outline-none focus:border-cyan-500" type="password" placeholder="admin" name="password" required />
					</div>
					<div class="mt-10">
						<button type="submit" class="bg-cyan-500 text-gray-100 p-4 w-full rounded-full tracking-wide
                          font-semibold font-display focus:outline-none focus:shadow-outline hover:bg-cyan-600
                          shadow-lg">
							Log In
						</button>
					</div>
					<?php echo form_close(); ?>
					<div class="mt-12 text-sm font-display font-semibold text-gray-700 text-center">
						Don't have an account ?
						<a class="cursor-pointer text-cyan-600 hover:text-cyan-800">
							Sign up
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="hidden lg:flex items-center justify-center bg-cyan-100 flex-1 h-screen">
			<div class="max-w-xs transform duration-200 hover:scale-110 cursor-pointer">
				<img class="rounded-lg" src="https://img.freepik.com/free-vector/mobile-login-concept-illustration_114360-135.jpg?w=1380&t=st=1662191620~exp=1662192220~hmac=35646fe95a2e8f609b1dcf0db8945c50966293e8910cff5f7d07880570b31628" alt="">
			</div>
		</div>
	</div>

</body>

</html>