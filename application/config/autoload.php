<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(E_PARSE);
ini_set('display_errors', 1);
$autoload['packages'] = array();
$autoload['libraries'] = array('database','session', 'form_validation','upload');
$autoload['drivers'] = array();
$autoload['helper'] = array('date','url', 'form');
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array();


